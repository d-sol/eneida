﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public GameObject respawnPoint;
	public GameObject player;
	public GameObject Didona;

	internal bool isGameOver;

	// Use this for initialization
	void Start () {
		 Time.timeScale = 1;
	}
	
	// Update is called once per frame
	void Update () {
		if (player.transform.position.y <= 0 || !player.activeSelf)
			isGameOver = true;
	}


	void OnGUI(){
		if (isGameOver) {
			Rect rect = new Rect(Screen.width / 2 - 100,Screen.height / 2 - 50, 200, 75); 
			GUI.Box(rect, "Game Over"); 
			Rect rect2 = new Rect (Screen.width / 2 - 30, Screen.height / 2 - 25, 60, 50); 
			GUI.Label (rect2, "Good Job!");
			Destroy (player);
			Destroy (Didona);
		}
	}
	
	public static void PauseGame (float pauseDuration)
	{
		Time.timeScale = 0; // pause
		float pauseEndTime = Time.realtimeSinceStartup + pauseDuration;
		while (Time.realtimeSinceStartup < pauseEndTime){}
		Time.timeScale = 1f; // restore time scale from before pause
	}

}
