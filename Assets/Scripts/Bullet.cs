﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.UIElements;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Vector3 target;

   // [SerializeField] 
    private float speed = 13;
    
    [SerializeField]
    [Range(1, 10)]
    private int damage = 1;
    
    private Camera cam;
    void Start()
    {
        cam = Camera.main;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, 200))
        {
            target = hitInfo.point;
        }
    }
    
    void Update()
    {
        if (Math.Abs((transform.position - target).magnitude) < 0.3)
        {
            Destroy(gameObject);
            return;
        }
        //TODO: what is slerp doing here?
        transform.position = Vector3.SlerpUnclamped(transform.position, target, speed*Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
        {
            var health = other.GetComponent<Health>();
            if (health != null)
                health.TakeDamage(damage);
            Destroy(gameObject);
        }
    }
}
