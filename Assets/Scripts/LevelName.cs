﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LevelName : MonoBehaviour
{
    public Image levelName;
    [SerializeField] 
    private AudioSource audioSource;

    void Start()
    {
        levelName.gameObject.SetActive(true);
        audioSource.Play();
        Destroy(levelName.gameObject, 2.5f);
    }
    
}
