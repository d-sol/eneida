﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Didona : MonoBehaviour {
	public float speed;
	public float stoppingDistance;
	public float retreatDistance;
	public Transform player;
	
	public float startTimeBtwShots;
	public GameObject projectile;
	private float _timeBtwShots;
	[SerializeField]
	private Transform firePoint;
	
	private Rigidbody rb;

	// Use this for initialization
	void Start ()
	{
		Time.timeScale = 1;
	//player = GameObject.Find ("eney").transform;
		_timeBtwShots = startTimeBtwShots;
		rb = GetComponent<Rigidbody> ();
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 targetXZPos = new Vector3(player.position.x, transform.position.y, player.position.z);
		transform.LookAt(targetXZPos);
		Move ();
		Shoot ();
	}

	
	void Move()
	{
		float distanceToPlayer = Vector3.Distance (transform.position, player.position);
		if ( distanceToPlayer > stoppingDistance) {
			rb.AddRelativeForce(Vector3.forward * speed, ForceMode.Force);
		} else if (distanceToPlayer > retreatDistance) {
			rb.velocity = Vector3. zero;
		} else {
			rb.AddRelativeForce(Vector3.forward * -speed, ForceMode.Force);
		}
	}

	void Shoot()
	{
		if (_timeBtwShots <= 0) 
		{
			Instantiate (projectile, firePoint.position, Quaternion.identity);
			_timeBtwShots = startTimeBtwShots;
		} else 
		{
			_timeBtwShots -= Time.deltaTime;
		}
	}


}
