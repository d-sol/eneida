﻿using UnityEngine;

public class Projectile : MonoBehaviour {

	private Vector3 _targetPos;
	public int throwingPower = 1;
	private Rigidbody _rigidbody;
	private Quaternion _startRotation;
	Vector3 newPos;
	private float launchAngle = 30;

	[SerializeField] 
	private GameObject fire;
	
	[SerializeField]
	private AudioSource breakingJarSource;
	
	// Use this for initialization
	void Start ()
	{
		Transform targetTransform = GameObject.Find ("Player").transform;
		_targetPos = new Vector3 (targetTransform.position.x, targetTransform.position.y-1, targetTransform.position.z);
		_rigidbody = GetComponent<Rigidbody> ();
		_startRotation = transform.rotation;
		transform.LookAt(_targetPos);
		float R = Vector3.Distance(transform.position, _targetPos);
		float G = Physics.gravity.y * 5f;
		float tanAlpha = Mathf.Tan(launchAngle * Mathf.Deg2Rad);
		float H = _targetPos.y +  - transform.position.y;
    
		// calculate initial speed required to land the projectile on the target object 
		float Vz = Mathf.Sqrt(G * R * R / (2.0f * (H - R * tanAlpha)) );
		float Vy = tanAlpha * Vz;
		
		// create the velocity vector in local space and get it in global space
		Vector3 localVelocity = new Vector3(0f, Vy, Vz);
		Vector3 globalVelocity = transform.TransformDirection(localVelocity);

		// launch the object by setting its initial velocity and flipping its state
		_rigidbody.velocity = globalVelocity;
	}
		
	// Update is called once per frame
	void Update () {
		transform.rotation = Quaternion.LookRotation(_rigidbody.velocity) * _startRotation;
		if (transform.position.y < 10)
		{
			DestroyProjectile();
		}
	}
		

	void DestroyProjectile(){
		Destroy(gameObject);
	}

	void OnTriggerEnter(Collider other) {
		if(other.CompareTag("Player")){
			// TODO: implement some kind of explosion throwing player away or hitting him
			other.GetComponent<Health>().TakeDamage(throwingPower);
		/*	Vector3 newPos = new Vector3 (player.position.x + (throwingPower*4)*(player.position.x - startPosition.x), 
				throwingPower*10, 
				player.position.z + (throwingPower*4)*(player.position.z - startPosition.z));
			player.position = Vector3.MoveTowards (player.position, newPos, 300 * Time.deltaTime);
			GameController.PauseGame(1);*/
		}

		if (other.CompareTag("Terrain"))
		{
			Instantiate(fire, transform.position, Quaternion.identity);
		}

		if (!other.CompareTag("enemy")){
			//TODO: write coroutine to play sound
			//if you just play it, the projectile is destroyed before any sound comes out
			//breakingJarSource.Play();
			DestroyProjectile();
			
		}
	}
	
	
}
