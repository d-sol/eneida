﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public GameObject player;       //Public variable to store a reference to the player game object


	private Vector3 offset;
	private float fixedY;

	// Use this for initialization
	void Start () 
	{
		offset = transform.position - player.transform.position;
		fixedY = transform.position.y;
	}

	void LateUpdate () 
	{
		Vector3 newPos = player.transform.position + offset;
		transform.position = new Vector3(newPos.x, fixedY, newPos.z);
	}
}
