﻿using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField]
    [Range(0.1f, 1.5f)]
    private float fireRate = 0.3f;

    [SerializeField]
    private Transform firePoint;

    [SerializeField]
    private GameObject bullet;

 //   [SerializeField]
 //   private ParticleSystem muzzleParticle;

    [SerializeField]
    private AudioSource gunFireSource;

    private float timer;
    
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= fireRate)
        {
            if (Input.GetButton("Fire1"))
            {
                timer = 0f;
                FireGun();
            }
        }
    }

    private void FireGun()
    {
      //  muzzleParticle.Play();
        gunFireSource.Play();
        Instantiate(bullet, firePoint.position, Quaternion.identity);
    }
}
