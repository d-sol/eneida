﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

public class Player : MonoBehaviour {
	private Rigidbody _rigidbody;
	
	[SerializeField]
	private float speed = 20f;
	[SerializeField]
	private float jumpSpeed = 30f;
	[SerializeField]
	private float jumpHeight = 4f;
	[FormerlySerializedAs("_fallSpeed")] [SerializeField]
	private float fallSpeed = 35f;
	[SerializeField]
	private float turnSpeed = 5f;
	
	private Vector3 _moveVelocity;
	private Quaternion _rotation;
	private Vector3 _jumpVelocity;
	
	private bool _isGrounded = true;
	private float _groundedYPos;
	
	//private Transform enemy;
	private Health health;
	private Animator animator;
	
	//public GameObject gameController;
	//private GameController gameControllerScript;
	
	// Use this for initialization
	void Start () {
	Time.timeScale = 1f;
		_rigidbody = GetComponent<Rigidbody> ();
		animator = GetComponentInChildren<Animator>();
		health = GetComponent<Health>();
		//	gameControllerScript = gameController.GetComponent<GameController> ();
		//	enemy = GameObject.Find ("Didona").transform;
	}

	// Update is called once per frame
	void Update () {
		Vector3 moveInput = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
		animator.SetFloat("speed", moveInput.magnitude);
		ProcessMovement(moveInput);
		ProcessRotation(moveInput);
		ProcessJump();
		Shoot();
	}

	private void ProcessMovement(Vector3 moveInput)
	{
		_moveVelocity = moveInput.normalized * speed;
		
	}

	private void ProcessRotation(Vector3 moveInput)
	{
		if (moveInput.magnitude <= 0) return;
		var newDirection = Quaternion.LookRotation(moveInput);
		_rotation = Quaternion.Slerp(transform.rotation, newDirection, Time.deltaTime * turnSpeed);
	}

	private void ProcessJump()
	{ //TODO: refactor
		//TODO: sometimes height of jump is different somehow
		if (Input.GetButtonDown("Jump") && _isGrounded)
		{
			_jumpVelocity = Vector3.up * -Physics.gravity.y * jumpSpeed * Time.deltaTime;
			_isGrounded = false;
		}
		else if (transform.position.y > _groundedYPos + jumpHeight)
		{
			_jumpVelocity = Vector3.up * Physics.gravity.y * fallSpeed * Time.deltaTime;
			_isGrounded = false;
		}
		else
		{
			_isGrounded = true;
		}
	}

	private void Shoot()
	{
	/*	if (Input.GetButton("Fire1")) 
		{
			var pos = new Vector3 (transform.position.x, enemy.position.y + 7, transform.position.z);
			Instantiate (projectile, pos, Quaternion.identity);
		} */
	}

	private void FixedUpdate() {
		_rigidbody.MoveRotation(_rotation);
		_rigidbody.MovePosition (_rigidbody.position + _moveVelocity * Time.deltaTime);
		_rigidbody.AddForce(_jumpVelocity, ForceMode.Impulse);
	}
	
	private void OnCollisionEnter(Collision col)
	{
		
		// some player collisions with ground mess with jumps
		// probably when not all points have same y, but he still lands to the ground, it doesn't work and thus problems
		if (col.gameObject.CompareTag("Terrain"))
		{
			var firstPointY = col.contacts[0].point.y;
			var isHorizontalPlane = col.contacts.All(p => p.point.y == firstPointY);
			if (isHorizontalPlane)
			{
				_isGrounded = true;
				_groundedYPos = transform.position.y;
			}
		}

		if (col.gameObject.CompareTag("fire"))
		{
			health.TakeDamage(2);
		}
	}
		

}
